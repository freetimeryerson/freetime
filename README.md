# Freetime Web Application #

**Current Contributors**

* Mark Stanley

* Rogerio Ribeiro

* Dominic Aidoo

**About**

Freetime was a group project for our Web Applications course at Ryerson University. Freetime helps solve the problem of scheduling group meetings.
Using each member's Gmail calendar Freetime will display the common free times within the group. From there the group's moderator is able to select a final meeting time and all other members are able to add the meeting to their Gmail calendar.

**Technologies Used**

* NodeJS

* ExpressJS

* MongoDB

* PassportJS

* JQuery

* Bootstrap

**Current Status**

We are currently implementing a new, fresh, and responsive redesign of our UI.

Here are a couple screenshots of the UI:

![Profile Page](https://preview.ibb.co/nax2bF/Screen_Shot_2017_02_27_at_12_12_06_PM.png)
![Group Page](https://preview.ibb.co/hKneOv/Screen_Shot_2017_02_27_at_12_12_16_PM.png)
![Attend Meeting Page](https://preview.ibb.co/cde4qa/Screen_Shot_2017_02_27_at_12_12_34_PM.png)
![Choose Meeting Time Page](https://preview.ibb.co/iGoxAa/Screen_Shot_2017_02_27_at_12_12_51_PM.png)
![Add to Calendar Page](https://preview.ibb.co/k5N2bF/Screen_Shot_2017_02_27_at_12_13_23_PM.png)
