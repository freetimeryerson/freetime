var mongoose = require('mongoose');

// An event occurs when: group/meeting is created or final meeting time is set.
var eventSchema = mongoose.Schema({
  type: String, // "group" || "meeting" || "final"
  createdBy: {
    email: String,
    name: String
  },
  ref_id: String, // reference to the id of the group/meeting
  ref_name: String, // reference to the name of the group/meeting
  viewed: Boolean
});

module.exports = mongoose.model('Event', eventSchema);
