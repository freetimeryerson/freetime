var mongoose = require('mongoose');
var groupSchema = mongoose.Schema({
		name: String,
		members: [{	email: String,
								name: String}],
		meetings:[{	meetingName:String,
								startDay: String,
								endDay: String,
								startTime: String,
								endTime: String,
								meetingID:String,
								duration:Number,
								location: String,
								timeMax:String,
								timeMin:String,
								timeString:{
									start:{
										string: String,
										dayOfWeek: String,
										dayOfMonth: String,
										month: String,
										year: String,
										time: String
									},
									end: {
										string: String,
										dayOfWeek: String,
										dayOfMonth: String,
										month: String,
										year: String,
										time: String
									},
									string: String
								},
								final:Boolean}]
});
module.exports = mongoose.model('Group', groupSchema);
