var mongoose = require('mongoose');
var MeetingSchema = mongoose.Schema({
    name: String,
    group: String,
    meetingMembers:[String],
    moderator: String,
    final: Boolean,
    startDay: String,
    endDay:String,
    startTime:String,
    endTime:String,
    timeMin:String,
    timeMax:String,
    timeString:{
      start:{
        string: String,
        dayOfWeek: String,
        dayOfMonth: String,
        month: String,
        year: String,
        time: String
      },
      end: {
        string: String,
        dayOfWeek: String,
        dayOfMonth: String,
        month: String,
        year: String,
        time: String
      },
      string: String
    },
    duration: Number,
    location: String,
    membersAccepted: [{ email: String,
                        busy: Array}],
    membersAcceptedList:[String],
    submittedUsers:[String]
});
module.exports = mongoose.model('Meeting', MeetingSchema);
