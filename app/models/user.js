var mongoose = require('mongoose');
var Event = require('./event.js');
var userSchema = mongoose.Schema({
	google: {
		id:String,
		token:String,
		email:String, // user's email
		name:String, // user's name
		connections:[String],// user's acquaintances (people added to previous groups)
		groups:[{	groupname: String,
							groupID: String,
							viewed: Boolean}], // groups that the user is apart of
		meetings: [{
			meetingID: String,
			groupID: String,
			groupName: String,
			name: String,
			timeMin: String,
			timeMax: String,
			timeString:{
				start:{
					string: String,
					dayOfWeek: String,
					dayOfMonth: String,
					month: String,
					year: String,
					time: String
				},
				end: {
					string: String,
					dayOfWeek: String,
					dayOfMonth: String,
					month: String,
					year: String,
					time: String
				},
				string: String
			},
			duration: Number,
			location: String,
			status: String // can either be: "Pending" / "ToAdd" / "Complete"
		}],
		events: [Event.schema]
	}
});
module.exports = mongoose.model('User', userSchema);
