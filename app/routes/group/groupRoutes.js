var User = require('./../../models/user');
var Group = require('./../../models/group');
var Meeting = require('./../../models/meeting');
var Event = require('./../../models/event');
var dateFormat = require('dateformat');
var moment = require('moment');

function isLoggedIn(req, res, next){
if(req.isAuthenticated())
    return next();
res.redirect('/');
}

module.exports = function(app,pusher){

  // Render group profile page
  app.get('/group/:groupID',isLoggedIn,function(req,res){
  	var groupID = req.params.groupID;
  	var user = req.user;
  	Group.findById(groupID, function(err,group){
      if(err){
        console.log(err);
      } else {
        //group.save();
        User.find().distinct('google.email', function (err, userEmails){
          res.render('groupProfile.ejs',{group: group, user: user, userList: userEmails, moment:moment,
            knownNetwork: req.user.google.connections, userEvents: req.user.google.events});
        });
      }
  	});
	});

  //Reset the user's notifcations
  /*app.get('/resetNotifs', function(req, res){
    var userEmail = req.user.google.email;
    var events = req.user.google.events;
    var i = req.user.google.events.length - 1;
    console.log(i);
    var query = { 'google.email': userEmail};

    User.findOne( query, function(err, user) {
      while (user.google.events[i].viewed == false) {
        user.google.events[i].viewed = true;
        if (i > 0) { i--; }
        user.save();
        console.log(user.google.events);
      }

      setTimeout(function(){
        res.redirect('..');
      }, 60000);

    });
  });*/

  // Create a new meeting for the group
  app.post('/group/:groupID/createMeeting',isLoggedIn,function(req,res){
    var groupID = req.params.groupID;
    var userEmail = req.user.google.email;
    var username = req.user.google.name;
    Group.findById(groupID, function(err,group){
      var newMeeting = new Meeting();
      var datetimes = req.body.daterange.split(" - ");
      var fbTimeMin = dateFormat(datetimes[0], "isoDateTime");
      var fbTimeMax = dateFormat(datetimes[1], "isoDateTime");
      var start = datetimes[0].split(" ");
      var end = datetimes[1].split(" ");

      var startDay = new Date(fbTimeMin);
      var endDay = new Date(fbTimeMax);

      //Set new Meeting fields
      newMeeting.name = req.body.meetingName;
      newMeeting.group = groupID;
      // TODO: Refactor for loop
      for (var x = 0; x < group.members.length;x++){
        newMeeting.meetingMembers.push(group.members[x].email);
      }
      newMeeting.moderator = userEmail;
      newMeeting.final = false;
      newMeeting.startDay = start[0];
      newMeeting.endDay = end[0];
      newMeeting.startTime = start[1] + start[2];
      newMeeting.endTime = end[1] + end[2];
      newMeeting.timeMax = fbTimeMax;
      newMeeting.timeMin = fbTimeMin;
      newMeeting.timeString.start.string = dateFormat(startDay,"h:MMTT dddd mmmm dS, yyyy");
      newMeeting.timeString.start.dayOfWeek = dateFormat(startDay,"dddd");
      newMeeting.timeString.start.dayOfMonth = dateFormat(startDay,"dS");
      newMeeting.timeString.start.month = dateFormat(startDay,"mmmm");
      newMeeting.timeString.start.year = dateFormat(startDay,"yyyy");
      newMeeting.timeString.start.time = dateFormat(startDay,"h:MMTT");
      newMeeting.timeString.end.string = dateFormat(endDay,"h:MMTT dddd mmmm dS, yyyy");
      newMeeting.timeString.end.dayOfWeek = dateFormat(endDay,"dddd");
      newMeeting.timeString.end.dayOfMonth = dateFormat(endDay,"dS");
      newMeeting.timeString.end.month = dateFormat(endDay,"mmmm");
      newMeeting.timeString.end.year = dateFormat(endDay,"yyyy");
      newMeeting.timeString.end.time = dateFormat(endDay,"h:MMTT");
      newMeeting.duration = req.body.meetingDuration;
      newMeeting.location = req.body.meetingLocation;

      // Save new Meeting
      newMeeting.save(function(err){
        if(err){
          console.log(err);
        } else {
          // Add meeting to group's meeting list
          group.meetings.push({
            meetingName: newMeeting.name,
            startDay: newMeeting.startDay,
            endDay: newMeeting.endDay,
            startTime: newMeeting.startTime,
            endTime: newMeeting.endTime,
            meetingID:newMeeting.id,
            timeMin: newMeeting.timeMin,
            timeMax: newMeeting.timeMax,
            timeString:{
              start:{
                string: newMeeting.timeString.start.string,
                dayOfWeek: newMeeting.timeString.start.dayOfWeek,
                dayOfMonth: newMeeting.timeString.start.dayOfMonth,
                month: newMeeting.timeString.start.month,
                year: newMeeting.timeString.start.year,
                time: newMeeting.timeString.start.time
              },
              end:{
                string: newMeeting.timeString.end.string,
                dayOfWeek: newMeeting.timeString.end.dayOfWeek,
                dayOfMonth: newMeeting.timeString.end.dayOfMonth,
                month: newMeeting.timeString.end.month,
                year: newMeeting.timeString.end.year,
                time: newMeeting.timeString.end.time
              }
            },
            duration: newMeeting.duration,
            location: newMeeting.location,
            final: false
          });
          group.save();



          var newEvent  = new Event({
                type: "meeting",
                createdBy: {
                  email: userEmail,
                  name: username
                },
                ref_id: group.id+"/"+newMeeting.id,
                ref_name: newMeeting.name,
                viewed: false
            });
            newEvent.save(function(err){
              if(err){
                console.log(err);
              } else {
                // for each member in this group...
                for(var i = 0; i < group.members.length; i++){
                  if (group.members[i].email != userEmail) {

                    User.findOne({"google.email": group.members[i].email},function(err, member){
                      // Notify the member that a new meeting was created
                      pusher.trigger(member.google.email, 'newEvent', {
                        "msg": "<li><a href=" + "/meeting/"+newEvent.ref_id +" \">" + newEvent.createdBy.name +" created the meeting "+newEvent.ref_name+"</a></li>",
                        "href": "/meeting/"+newEvent.ref_id,
                        "count": member.google.events.length,
                        "type": "meeting",
                        "eventName": newEvent.ref_name
                      });

                      //add the new event to the member's event[]
                      member.google.events.push(newEvent);

                      //add meeting to member's meeting[]
                      //TODO: redo implementation
                      var startDate = new Date(newMeeting.timeMin);
                      var endDate = new Date(newMeeting.timeMax);

                      member.google.meetings.push({
                        meetingID: newMeeting.id,
                        groupID: newMeeting.group,
                        groupName: group.name,
                        name: newMeeting.name,
                        timeMin: newMeeting.timeMin,
                        timeMax: newMeeting.timeMax,
                        timeString:{
                          start:{
                            string: newMeeting.timeString.start.string,
                            dayOfWeek: newMeeting.timeString.start.dayOfWeek,
                            dayOfMonth: newMeeting.timeString.start.dayOfMonth,
                            month: newMeeting.timeString.start.month,
                            year: newMeeting.timeString.start.year,
                            time: newMeeting.timeString.start.time
                          },
                          end:{
                            string: newMeeting.timeString.end.string,
                            dayOfWeek: newMeeting.timeString.end.dayOfWeek,
                            dayOfMonth: newMeeting.timeString.end.dayOfMonth,
                            month: newMeeting.timeString.end.month,
                            year: newMeeting.timeString.end.year,
                            time: newMeeting.timeString.end.time
                          }
                        },
                        duration: newMeeting.duration,
                        location: newMeeting.location,
                        status: "Pending"
                      });
                      member.save();
                    })
                  }
                }
              }
            });
          res.redirect('/meeting/' + groupID +"/" + newMeeting.id);
        }
      });
    });
  });

  //Remove the user from group
  app.post('/group/:groupID/removeUser', function (req, res) {
    var groupID = req.params.groupID;
    var userEmail = req.user.google.email;
    var user = req.user;
    var userGroups = user.google.groups;

    //Find group to remove user from, using groupID
    Group.findById(groupID, function(err, group) {
      var members = group.members;
      var index = members.indexOf(userEmail);

      //If there are only 2 people inside the group that the
      //current user wants to leave
      if (members.length < 3){
        //Remove all meetings relating to group, if any
        if (group.meetings.length > 0) {
          for (var i=0; i<group.meetings.length; i++) {
            Meeting.find({"_id": group.meetings[i].meetingID}).remove().exec();
          }
        }

        //Remove the group to be removed from each user's
        //list of groups
        for (var i=0; i<members.length; i++) {
          //Find the user, and loop through their groups
          //till you find the group to be removed, and
          //remove it
          User.findOne({"google.email": members[i]}, function (err, groupMember){
            var currMemberGroups = groupMember.google.groups;
            for (var j=0; j<currMemberGroups.length; j++) {
              if (groupMember.google.groups[j].groupID == groupID){
                groupMember.google.groups.splice(j, 1);
                groupMember.save();
                break;
              }
            }

          });
          //Delete the group from the database
          Group.find({"_id": group.id}).remove().exec();
        }
      }

      else {
        //First, remove the user from the meetings
        //associated with the group

        //If there are meetings for this group
        if (group.meetings.length > 0) {
          //For each meeting, remove the user from all relevant
          //parts of meeting
          for (var i=0; i<group.meetings.length; i++) {
            Meeting.findOne({"_id": group.meetings[i].meetingID}, function (err, meeting) {
              var meetingMembers = meeting.meetingMembers;
              var membersAccepted = meeting.membersAccepted;
              var mIndex = meetingMembers.indexOf(userEmail);

              //Remove the user from the meetingMembers array
              meetingMembers.splice(mIndex, 1);

              //If user is moderator, remove user from meetingMembers,
              //set a new moderator
              if (meeting.moderator == userEmail) {
                meeting.moderator = meetingMembers[0];
              }


              //If the membersAccepted array is not empy
              //check for the user and remove info
              if (meeting.membersAccepted.length > 0) {
                for (var j=0; j<meeting.membersAccepted.length; j++) {
                  if (membersAccepted[j].email == userEmail) {
                    membersAccepted.splice(j, 1);
                    break;
                  }
                }
              }
              meeting.meetingMembers = meetingMembers;
              meeting.membersAccepted = membersAccepted;
              meeting.save();

            });
          }
        }

        //Second, remove the user from relevant group info
        members.splice(index, 1);
        group.members = members;
        group.save();

        //Last, remove the group from the user's
        //list of groups
        for (var x=0; x<user.google.groups.length;x++) {
          if (userGroups[x].groupID == groupID){
            userGroups.splice(x, 1);
            user.google.groups = userGroups;
            user.save();
            break;
          }
        }
      }
    });

    res.redirect('/profile');
  });
}//end of exports
