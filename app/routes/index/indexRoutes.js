var User = require('./../../models/user');
var moment = require('moment');
function isLoggedIn(req, res, next){
if(req.isAuthenticated())
    return next();
res.redirect('/');
}

// General routes for app
module.exports = function(app){

  // home page
  app.get('/', function(req,res){
    res.render("index.ejs");
  });

  // help page
  app.get('/help',isLoggedIn,function(req,res){
      User.find().distinct('google.email', function (err, userEmails) {
        res.render('help.ejs',{user: req.user, knownNetwork: req.user.google.connections,
          userList:userEmails, moment: moment, userEvents: req.user.google.events});
      });
	});

  // Logout user
  app.get('/logout',isLoggedIn,function(req, res){
      req.logout();
      res.redirect('/');
  });
}
