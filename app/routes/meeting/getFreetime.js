var User = require('./../../models/user');
var Group = require('./../../models/group');
var Meeting = require('./../../models/meeting');
var Event = require('./../../models/event');

var dateFormat = require('dateformat');

function isLoggedIn(req, res, next){
if(req.isAuthenticated())
    return next();
res.redirect('/');
}

function dateToString(isoDate){
  var date = new Date(isoDate);
  date = date.toString();
  return date;
}

Date.prototype.addDays = function(days) {
  this.setDate( this.getDate()  + days);
  return this;
};

Date.prototype.addHours= function(hours){
    this.setHours(this.getHours()+hours);
    return this;
}

module.exports = function(app, pusher){

  // Render the getFreetime page
  //TODO: do a through check on this get Request
  app.get('/getFreetime/:groupID/:meetingID',isLoggedIn,function(req,res){
    //Find the days between start and end intervals
    var groupID = req.params.groupID;
    var meetingID = req.params.meetingID;

    Meeting.findById(meetingID, function(err, meeting){

        //Get the amount of days including and between the start and end date
        var oneDay = 24*60*60*1000;
        var firstDate = new Date(meeting.startDay);
        var secondDate = new Date(meeting.endDay);
        var diffDays = Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)) + 1;

        /*
            Create array representing every hour of each day within start and end intervals.
            true = no group member is busy during that hour
            false = at least one group member is busy during that hour
        */
        var n = diffDays * 24;
        var totalHours = [];
        for(var i = 0; i < n; i++){
            totalHours[i] = true;
        }

        // set all the hours prior to the starting hour (of the starting day) to false
        var startTime = meeting.startTime;
        var begTime = startTime.split(':');
        var startHour;
        if (parseInt(begTime[0]) == 12) {
          if (begTime[1].substring(2,2) == 'P') {
            startHour = 12;
          } else {
            startHour = 0;
          }
        }
        else if (begTime[1].substring(2,2) == 'P'){
          startHour = parseInt(begTime[0]) + 12;
        } else {
          startHour = parseInt(begTime[0]);
        }
        //Parse the startHour
        /* if (parseInt(startTime.substring(0,1)) == 0) {
          var startHour = startTime.substring(1,1);
          console.log("this one0");
        }

        else {
          var startHour = startTime.substring(0,2);
        } */
        for(var i = 0; i < startHour; i++){
          totalHours[i]= false;
        }
        // set all the hours after the ending hour to false
        var endTime = meeting.endTime;
        var endHour = endTime.substring(0,2);
        var endHour = parseInt(endHour) + ((diffDays - 1) * 24);
        for(var i = n-1; i >= endHour; i--){
          totalHours[i]= false;
        }
        //go through each user's busy time and set value to false when necessary
        numberOfMembers = meeting.membersAccepted.length;
        for(var i = 0; i < numberOfMembers; i++){
          numberOfEvents = meeting.membersAccepted[i].busy.length;
          for(var j = 0; j < numberOfEvents; j++ ){
            // get the start and end time of that event
            // separate it into two parts -> date and time
            // splits the string at the T
            // [0] = the date
            // [1] = the time
            s = meeting.membersAccepted[i].busy[j].start.split("T");
            e = meeting.membersAccepted[i].busy[j].end.split("T");
            // START
            s[0] = new Date(s[0]);
            diffDays = Math.round(Math.abs((firstDate.getTime() - s[0].getTime())/(oneDay)));
            // [0] = hour
            // [1] = min
            var sTime = s[1].substring(0,5).split(":");
            var sHour = parseInt(sTime[0]);
            var sMin = parseInt(sTime[1]);
            var sTakeOut = sHour + (diffDays * 24);

            // END
            e[0] = new Date(e[0]);
            diffDays = Math.round(Math.abs((firstDate.getTime() - e[0].getTime())/(oneDay)));
            // [0] = hour
            // [1] = min
            var eTime = e[1].substring(0,5).split(":");
            var eHour = parseInt(eTime[0]);
            var eMin = parseInt(eTime[1]);
            if(eMin > 0){
              eHour = eHour + 1;
            }
            var eTakeOut = eHour + (diffDays * 24);
            for(var k = sTakeOut; k < eTakeOut; k++){
            //for(var k = sTakeOut; k <= eTakeOut; k++){
              totalHours[k] = false;
            }
          }
        }
        var startArray= [];
        var endArray = [];
        for(var i = 0; i < n; i++) {
          if(totalHours[i] == true){
            startArray.push(i);
            for(var j = i; j < n; j++){
              if(totalHours[j] == false || j == (n-1)){
                endArray.push(j-1);
                i = j;
                break;
              }
            }
          }
        }
        var startDayArray = [];
        var endDayArray = [];
        for(var i = 0; i < startArray.length; i++){
            //duration check
            var sHour = startArray[i];
            var eHour = endArray[i];
            var duration = meeting.duration;
            if((eHour - sHour) >= duration){
              //START: Transform into Day String
              var sDaysAway = Math.floor(sHour/24);
              sHour = sHour - (sDaysAway * 24);
              var fDate = new Date(meeting.startDay);
              var sDay = fDate.addDays(sDaysAway).addHours(sHour);
              sDay = sDay.toString().substring(0,21);
              sDayHour = parseInt(sDay.substring(15,18));
              var sDayHour_String;
              if(sDayHour == 0){
                sDayHour_String = "12:00AM";
              } else if (sDayHour > 0 && sDayHour < 13){
                sDayHour_String = sDayHour +":00AM";
              } else {
                sDayHour = sDayHour - 12;
                sDayHour_String = sDayHour + ":00PM";
              }
              sDay_String = sDay.substring(0,15);
              sFinal = sDay_String + " " + sDayHour_String;
              startDayArray.push(sFinal);
              var eDaysAway = Math.floor(eHour/24);
              eHour = eHour - (eDaysAway * 24);
              var fDate = new Date(meeting.startDay);
              var eDay = fDate.addDays(eDaysAway).addHours(eHour);
              eDay = eDay.toString().substring(0,21);
              eDayHour = parseInt(eDay.substring(15,18)) + 1;
              var eDayHour_String;
              if(eDayHour == 0){
                eDayHour_String = "12:00AM";
              } else if (eDayHour > 0 && eDayHour < 13){
                eDayHour_String = eDayHour +":00AM";
              } else {
                eDayHour = eDayHour - 12;
                eDayHour_String = eDayHour + ":00PM";
              }
              eDay_String = eDay.substring(0,15);
              eFinal = eDay_String + " " + eDayHour_String;
              endDayArray.push(eFinal);
            }
        }
        User.find().distinct('google.email', function (err, userEmails) {
          Group.findById(groupID, function(err, group){
            res.render('getFreetime.ejs',{knownNetwork: req.user.google.connections, group:group, userList:userEmails,
              startDays: startDayArray, endDays: endDayArray, user: req.user, userEvents: req.user.google.events,
              meeting: meeting, groupID: groupID});
          });
        });
        /*
        for(var i = 0; i < startDayArray.length; i++){
                console.log("Possible Meeting Slot: " + i);
                console.log("START TIME: " + startDayArray[i]);
                console.log("END TIME:  " + endDayArray[i]);
        }
        */
       // for(var i = 0; i < n; i++) {
         //   console.log(i + " " + totalHours[i]);
       // }
    });
  });

  // Moderator submits the final meeting time to finalize the meeting
  app.post('/getFreetime/:groupID/:meetingID',function(req,res){
      var groupID = req.params.groupID;
      var meetingID = req.params.meetingID;
      var start = req.body.MeetingTime;

      Group.findById(groupID, function(err, group){
          //Update meeting information under Group's meetings list
          for(var x = 0; x < group.meetings.length; x++){
              if(meetingID == group.meetings[x].meetingID){

                  var startTime = new Date(start);

                  group.meetings[x].timeMin = startTime.toString();
                  group.meetings[x].timeString.start.string = dateFormat(startTime, "h:MMTT dddd mmmm dS, yyyy");
                  group.meetings[x].timeString.start.dayOfWeek = dateFormat(startTime,"dS");
                  group.meetings[x].timeString.start.dayOfMonth = dateFormat(startTime,"dddd");
                  group.meetings[x].timeString.start.month = dateFormat(startTime, "mmmm");
                  group.meetings[x].timeString.start.year = dateFormat(startTime,"yyyy");
                  group.meetings[x].timeString.start.time = dateFormat(startTime,"h:MMTT");

                  var endTime = startTime.addHours(group.meetings[x].duration);

                  group.meetings[x].timeMax = endTime.toString();
                  group.meetings[x].timeString.end.string = dateFormat(endTime,"h:MMTT dddd mmmm dS, yyyy");
                  group.meetings[x].timeString.end.dayOfWeek = dateFormat(endTime,"dS");
                  group.meetings[x].timeString.end.dayOfMonth = dateFormat(endTime,"dddd");
                  group.meetings[x].timeString.end.month = dateFormat(endTime,"mmmm");
                  group.meetings[x].timeString.end.year = dateFormat(endTime,"yyyy");
                  group.meetings[x].timeString.end.time = dateFormat(endTime,"h:MMTT");
                  group.meetings[x].final = true;
              }
          }
          //change the meeting status for all members to "ToAdd"
          for(var x = 0; x < group.members.length;x++){
            User.findOne({"google.email": group.members[x].email}, function (err, groupMember){
              for(var i = 0; i < groupMember.google.meetings.length; i++){
                if(groupMember.google.meetings[i].meetingID == meetingID){
                  var startTime = new Date(start);

                  groupMember.google.meetings[i].timeMin = startTime.toString();
                  groupMember.google.meetings[i].timeString.start.string = dateFormat(startTime, "h:MMTT dddd mmmm dS, yyyy");
                  groupMember.google.meetings[i].timeString.start.dayOfWeek = dateFormat(startTime,"dS");
                  groupMember.google.meetings[i].timeString.start.dayOfMonth = dateFormat(startTime,"dddd");
                  groupMember.google.meetings[i].timeString.start.month = dateFormat(startTime, "mmmm");
                  groupMember.google.meetings[i].timeString.start.year = dateFormat(startTime,"yyyy");
                  groupMember.google.meetings[i].timeString.start.time = dateFormat(startTime,"h:MMTT");

                  var endTime = startTime.addHours(groupMember.google.meetings[i].duration);

                  groupMember.google.meetings[i].timeMax = endTime.toString();
                  groupMember.google.meetings[i].timeString.end.string = dateFormat(endTime,"h:MMTT dddd mmmm dS, yyyy");
                  groupMember.google.meetings[i].timeString.end.dayOfWeek = dateFormat(endTime,"dS");
                  groupMember.google.meetings[i].timeString.end.dayOfMonth = dateFormat(endTime,"dddd");
                  groupMember.google.meetings[i].timeString.end.month = dateFormat(endTime,"mmmm");
                  groupMember.google.meetings[i].timeString.end.year = dateFormat(endTime,"yyyy");
                  groupMember.google.meetings[i].timeString.end.time = dateFormat(endTime,"h:MMTT");

                  groupMember.google.meetings[i].status = "ToAdd";
                  groupMember.save();
                }
              }
            });
          };
          group.save(function(err){
            if(err){
              console.log(err)
            } else {
              Meeting.findById(meetingID, function(err, meeting){
                  //edit the meeting information
                  var startTime = new Date(start);
                  meeting.timeMin = startTime.toString();
                  meeting.timeString.start.string = dateFormat(startTime, "h:MMTT dddd mmmm dS, yyyy");
                  meeting.timeString.start.dayOfWeek = dateFormat(startTime,"dS");
                  meeting.timeString.start.dayOfMonth = dateFormat(startTime,"dddd");
                  meeting.timeString.start.month = dateFormat(startTime, "mmmm");
                  meeting.timeString.start.year = dateFormat(startTime,"yyyy");
                  meeting.timeString.start.time = dateFormat(startTime,"h:MMTT");
                  var endTime = startTime.addHours(meeting.duration);
                  meeting.timeMax = endTime.toString();
                  meeting.timeString.end.string = dateFormat(endTime,"h:MMTT dddd mmmm dS, yyyy");
                  meeting.timeString.end.dayOfWeek = dateFormat(endTime,"dS");
                  meeting.timeString.end.dayOfMonth = dateFormat(endTime,"dddd");
                  meeting.timeString.end.month = dateFormat(endTime,"mmmm");
                  meeting.timeString.end.year = dateFormat(endTime,"yyyy");
                  meeting.timeString.end.time = dateFormat(endTime,"h:MMTT");
                  meeting.final = true;
                  meeting.save();

                  // create a new event object

                  var userEmail = req.user.google.email;
                  var username = req.user.google.name;
                  var newEvent = new Event({
                    type: "final",
                    createdBy: {
                      email: userEmail,
                      name: username
                    },
                    ref_id: meetingID,
                    ref_name: meeting.name,
                    viewed: false
                  });
                  newEvent.save(function(err){
                    if(err){
                      console.log(err);
                    } else {
                      for(var i = 0; i < group.members.length; i++){
                        if (group.members[i].email != userEmail) {
                          User.findOne({"google.email": group.members[i].email}, function (err, groupMember){
                            if (err)
                              return done(err);
                            else {
                              groupMember.google.events.push(newEvent);
                              groupMember.save(function(err){
                                if(err){
                                  console.log(err);
                                } else {
                                  pusher.trigger(groupMember.google.email, 'newEvent', {
                                    "msg": "<li><a> A date & time has been finalized for "+newEvent.ref_name+"</a></li>",
                                    "href": "/meetingConfirmation/"+group.id+"/"+newEvent.ref_id,
                                    "count": groupMember.google.events.length,
                                    "type": "final"
                                  });
                                }
                              });
                            }
                          });
                        }
                      };
                    }
                  });
              });
            }
          });
        });


      res.redirect('/meetingConfirmation/'+groupID+"/"+meetingID);
  });

  /*// Render page for the user to add meeting to calendar
  // TODO: change route to '/getFreetime/:groupID/:meetingID/addMeeting'
  app.get('/getFreetime/:groupID/:meetingID/addMeeting',isLoggedIn,function(req,res) {
      var groupID = req.params.groupID;
      var startDayArray = [];
      var endDayArray = [];
      var meetingID = req.params.meetingID;
      Meeting.findById(meetingID, function(err, meeting){
        User.find().distinct('google.email', function(err, groupEmails){
          Group.findById(groupID, function(err, group){
            res.render('getFreetime.ejs',{group:group, userList: groupEmails, startDays: startDayArray, endDays: endDayArray, user: req.user, meeting: meeting, groupID: groupID});
          });
        });
      });
  });*/

  //add final meeting details to user' google calendar
  // TODO: change route to "/getFreetime/:groupID/:meetingID/submit"
  app.post('/submitted/:meetingID',function(req,res) {
      var startDayArray = [];
      var endDayArray = [];
      var meetingID = req.params.meetingID;
      var user = req.user.google;
      Meeting.findById(meetingID, function(err, meeting){
          meeting.submittedUsers.push(req.user.google.email);
          meeting.save();
          res.redirect('/getFreetime/'+meetingID);
      });

      User.findOne({"google.email": user.email},function(err, member){
        for(var x = 0; x < member.google.meetings.length; x++){
          if(member.google.meetings[x].meetingID == meetingID){
            member.google.meetings[x].status = "Complete";
            member.save();
          }
        }
      })
  });
}
