var User = require('./../../models/user');
var Group = require('./../../models/group');
var Meeting = require('./../../models/meeting');

function isLoggedIn(req, res, next){
if(req.isAuthenticated())
    return next();
res.redirect('/');
}

module.exports = function(app){
//Render the meetingConfirmation page
  app.get('/meetingConfirmation/:groupID/:meetingID', isLoggedIn, function(req, res){
    var groupID = req.params.groupID;
    var startDayArray = [];
    var endDayArray = [];
    var meetingID = req.params.meetingID;
    Meeting.findById(meetingID, function(err, meeting){
      User.find().distinct('google.email', function(err, groupEmails){
        Group.findById(groupID, function(err, group){
          res.render('meetingConfirmation.ejs',{knownNetwork: req.user.google.connections, group:group, userList: groupEmails,
            startDays: startDayArray, endDays: endDayArray, user: req.user,
            userEvents: req.user.google.events, meeting: meeting, groupID: groupID});
        });
      });
    });
  });
}
