var User = require('./../../models/user');
var Group = require('./../../models/group');
var Meeting = require('./../../models/meeting');
var dateFormat = require('dateformat');

function isLoggedIn(req, res, next){
if(req.isAuthenticated())
    return next();
res.redirect('/');
}

module.exports= function(app){

  // Render meeting page
  app.get('/meeting/:groupID/:meetingID',isLoggedIn,function(req,res){
    var groupID = req.params.groupID;
    var meetingID = req.params.meetingID;
    var user = req.user;
    Meeting.findById(meetingID, function(err,meeting){
      Group.findById(groupID, function (err, group){
        // load our meeting page template using the meeting found in query.
        //var start = new Date(meeting.timeMin);
        //start = dateFormat(start, "dddd, mmmm dS, yyyy, h:MM:ss TT");
        //var end = new Date(meeting.timeMax);
        //end = dateFormat(end, "dddd, mmmm dS, yyyy, h:MM:ss TT");
        User.find().distinct('google.email', function (err, userEmails){
          res.render('meetingPage.ejs',{knownNetwork: req.user.google.connections, meeting: meeting, user: user, userList: userEmails, groupID: groupID,
            group: group, userEvents: req.user.google.events});
        });

      });
        });
  });

  // Submit member's freetime to the meeting
  app.post('/meeting/:meetingID/addUserFreetime', function (req, res) {
    var meetingID = req.params.meetingID;
    var userBusy = req.body.busy;
    var userEmail = req.user.google.email;
    Meeting.findById(meetingID, function(err,meeting){
      meeting.membersAcceptedList.push(userEmail);
      meeting.membersAccepted.push({email:userEmail, busy: userBusy});
      meeting.save();
    });
  });

    //Removal of a user from a meeting
    app.post('/meeting/:groupID/:meetingID/removeUser', function(req,res) {
      var user = req.user;
      var meetingID = req.params.meetingID;
      var groupID = req.params.groupID;
      var userToRemove = user.google.email;

      Meeting.findById(meetingID, function (err, meeting){
        var meetingMems = meeting.meetingMembers;
        var acceptedMems = meeting.membersAccepted;
        var index = meetingMems.indexOf(userToRemove);
        meetingMems.splice(index, 1);

        //If there were only 2 people scheduled for the meeting
        if (meetingMems.length < 2)
        {
          //Search for the group that the meeting is in
          Group.findById(groupID, function (err, group) {
            var meetingsForGroup = group.meetings;

            //Remove the meeting from the group's list of meetings
            for (var i=0; i<meetingsForGroup.length; i++) {
                if (meetingsForGroup[i].meetingID == meetingID) {
                  group.meetings.splice(i, 1);
                  break;
                }
            }
            group.save();

          });

          //Delete the meeting and save the updated group info
          Meeting.find({"_id": meeting.id}).remove().exec();
        }

        //Else just remove the user's info from the meeting
        else {
          //If user is moderator, remove user from meetingMembers,
          //set a new moderator
          if (meeting.moderator == userToRemove) {
            meeting.moderator = meetingMems[0];
          }

          //If a member of the meeting has authorized calendar access,
          //check to see if it is the current user, and remove the info
          if (acceptedMems.length > 0) {
            for  (var k=0; k<acceptedMems.length; k++) {
              if (acceptedMems[k].email == userToRemove) {
                acceptedMems.splice(k, 1);
                break;
              }
            }
          }

          meeting.meetingMembers = meetingMems;
          meeting.acceptedMembers = acceptedMems;
          meeting.save();
        }

      });

      res.redirect('/profile');
    });

}
