var User = require('./../../models/user');
var Group = require('./../../models/group');
var Meeting = require('./../../models/meeting');
var Event = require('./../../models/event');
var moment = require('moment');

function isLoggedIn(req, res, next){
if(req.isAuthenticated())
    return next();
res.redirect('/');
}

module.exports = function(app,pusher){
  // Render profile page
  app.get('/profile',isLoggedIn,function(req,res){
      User.find().distinct('google.email', function (err, userEmails) {
        res.render('profile.ejs',{user: req.user, knownNetwork: req.user.google.connections,
          userList:userEmails, moment: moment, userEvents: req.user.google.events});
      });
	});
  //Reset the user's notifcations
  /*app.get('/resetNotifs', function(req, res){
    var userEmail = req.user.google.email;
    var events = req.user.google.events;
    var i = req.user.google.events.length - 1;
    console.log(i);
    var query = { 'google.email': userEmail};

    User.findOne( query, function(err, user) {
      while (user.google.events[i].viewed == false) {
        user.google.events[i].viewed = true;
        if (i > 0) { i--; }
        user.save();
        console.log(user.google.events);
      }

      setTimeout(function(){
        res.redirect('..');
      }, 60000);

    });
  });*/



  // Create a new group
	app.post('/profile/createGroup',function(req,res){
    var user = req.user;
		var groupName = req.body.groupName;
    var tags = req.body.tags.split(",");

    var groupEmails = [];
		var userEmail = user.google.email;
    var username = user.google.name;
    var connections = user.google.connections;

    var newGroup = new Group();

		newGroup.name = groupName;
		newGroup.members.push({email:userEmail, name: username});

    //add all other user emails/names to the group
    //remove any extra whitespace from tags, and remove empty elements
    for (i=0; i<tags.length; i++) {
      tags[i] = tags[i].trim();

      if (tags[i] != "") {
        groupEmails[i] = tags[i];
      }
    }
    for(i = 0; i <groupEmails.length; i++){
      User.findOne({"google.email": groupEmails[i]}, function (err, groupMember){
        if(err)
          return done(err);
        else {
          newGroup.members.push({email: groupMember.google.email, name: groupMember.google.name});
          console.log("members: " + newGroup.members);
          // If the groupMember is not known already to the user:
          // Add groupMember to user's connections and vice-versa
          if (connections.indexOf(groupMember.google.email) < 0){
            user.google.connections.push(groupMember.google.email);
            groupMember.google.connections.push(user.google.email);

            user.save(function(error){if(error){console.log(error)}});
            groupMember.save(function(error){if(error){console.log(error)}});
          }
          // save newly created Group
          newGroup.save(function(err){
            if(err){
              console.log(err);
            } else {
              // create new event object
              var newEvent  = new Event({
                    type: "group",
                    createdBy: {
                      email: userEmail,
                      name: username
                    },
                    ref_id: newGroup.id,
                    ref_name: newGroup.name,
                    viewed: false
                  });

              newEvent.save(function(err){
                if(err){ console.log(err)} else {
                  //add new group and new event to all other members
                  for(x=0; x<groupEmails.length; x++){
                    User.findOne({"google.email": groupEmails[x]}, function (err, groupMember){
                      if (err)
                        return done(err);
                      else {

                        groupMember.google.events.push(newEvent);
                        groupMember.google.groups.push({groupname: groupName, groupID: newGroup.id});
                        //Check through group member's emails, to see if any are new connections
                        for (y=0; y<groupEmails.length; y++) {
                          //If the groupMember is known already to the user, or the groupMember themself, don't do anything
                          if (groupMember.google.connections.indexOf(groupEmails[y]) > -1 || groupMember.google.email ==  groupEmails[y]) {}
                          else {
                            //Add groupMember to user's network of known people using Freetime
                            groupMember.google.connections.push(groupEmails[y]);
                          }
                        }
                        groupMember.save(function(err){
                          if(err){
                            console.log(err)
                          } else{

                          pusher.trigger(groupMember.google.email, 'newEvent', {
                            "msg": "<li><a>"+ newEvent.createdBy.name +" added you to the group "+newEvent.ref_name+"</a></li>",
                            "href": "/group/" + newEvent.ref_id,
                            "count": groupMember.google.events.length,
                            "eventName": newEvent.ref_name,
                            "type": "group"
                          });
                        };
                      });
                      }
                    });
                  }
                };
              });
              // add new group to user
              user.google.groups.push({groupname: groupName, groupID: newGroup.id});
              user.save();

            }
            });
        }
      });
    }

    setTimeout(function(){
      res.redirect('/group/'+newGroup.id);
    }, 2000);
	});

  //Remove the current user from the database
  //TODO: code check
	app.post('/profile/deleteProfile', function(req, res){
		var user = req.user;
		var groups = user.google.groups;
		var userToRemove = req.user.google.email;

		//Remove the user from all the groups they are apart of
		for (var i=0; i<groups.length; i++){

			//Find group to remove user from, using groupID
			Group.findById(groups[i].groupID, function(err, group) {
				var members = group.members;
				var index = members.indexOf(userToRemove);
				var groupID = group.id;

				//If there are only 2 people inside the group that the current user wants to be removed
				if (members.length < 3){

          //Remove all meetings relating to group, if any
					if (group.meetings.length > 0) {
						for (var i=0; i<group.meetings.length; i++) {
							Meeting.find({"_id": group.meetings[i].meetingID}).remove().exec();
						}
					}

					//Remove the group to be removed from each user's list of groups
					for (var i=1; i<members.length; i++) {
						//Find the user, and loop through their groups
						//till you find the group to be removed, and
						//remove it
						User.findOne({"google.email": members[i]}, function (err, groupMember){
							var currMemberGroups = groupMember.google.groups;
							for (var j=0; j<currMemberGroups.length; j++) {
								if (groupMember.google.groups[j].groupID == groupID){
									groupMember.google.groups.splice(j, 1);
									groupMember.save();
									break;
								}
							}

						});
					}
					//Delete the group from the database
					Group.find({"_id": group.id}).remove().exec();
				} else {
					//First, remove the user from the meetings
					//associated with the group

					//If there are meetings for this group
					if (group.meetings.length > 0) {
						//For each meeting, remove the user from all relevant
						//parts of meeting
						for (var i=0; i<group.meetings.length; i++) {
							Meeting.findOne({"_id": group.meetings[i].meetingID}, function (err, meeting) {
								var meetingMembers = meeting.meetingMembers;
								var membersAccepted = meeting.membersAccepted;
								var mIndex = meetingMembers.indexOf(userToRemove);

								//Remove the user from the meetingMembers array
								meetingMembers.splice(mIndex, 1);

								//If user is moderator, remove user from meetingMembers,
								//set a new moderator
								if (meeting.moderator == userToRemove)
									meeting.moderator = meetingMembers[0];

								//If the membersAccepted array is not empy
								//check for the user and remove info
								if (meeting.membersAccepted.length > 0) {
									for (var j=0; j<meeting.membersAccepted.length; j++) {
										if (membersAccepted[j].email == userToRemove) {
											membersAccepted.splice(j, 1);
											break;
										}
									}
								}
								meeting.meetingMembers = meetingMembers;
								meeting.membersAccepted = membersAccepted;
								meeting.save();

							});
						}
					}
					//Second, remove the user from relevant group info
					members.splice(index, 1);
					group.members = members;
					group.save();

				}
			});
		}
		//Remove User from database, effectively deleting user's FreeTime account
		User.find({"google.email": user.google.email}).remove().exec();
		res.redirect('/');
	});
}// end of exports
