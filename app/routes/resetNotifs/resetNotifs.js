var User = require('./../../models/user');
var Event = require('./../../models/event');
var moment = require('moment');

module.exports = function(app, pusher){
  //Reset the user's notifcations
  app.get('/resetNotifs', function(req, res){
    var userEmail = req.user.google.email;
    var events = req.user.google.events;
    var i = req.user.google.events.length - 1;
    console.log(i);
    var query = { 'google.email': userEmail};

    User.findOne( query, function(err, user) {
      while (user.google.events[i].viewed == false) {
        user.google.events[i].viewed = true;
        if (i > 0) { i--; }
        user.save();
        console.log(user.google.events);
      }


    });
    res.json({message: "successful"});
  });
}
