module.exports = function(app, passport, pusher){
	// Route Paths
	require('./index/indexRoutes')(app,pusher);
	require('./profile/profileRoutes')(app,pusher);
	require('./group/groupRoutes')(app,pusher);
	require('./meeting/meetingRoutes')(app);
	require('./meeting/getFreetime')(app,pusher);
	require('./meeting/meetingConfirmation')(app);
	require('./auth/googleAuth')(app, passport);
	require('./resetNotifs/resetNotifs')(app, pusher);
}
