// Since the user is logged in via Freetime login there will be no need for an auth button but just incase there is one implemented.
// It allows FullCalendar to use the user's private calendar without making it public.
// Your Client ID can be retrieved from your project in the Google
// Developer Console, https://console.developers.google.com
var CLIENT_ID = '813433863993-dn497fmkof7a11h31rt6n7e1tubm2t68.apps.googleusercontent.com';

var SCOPES = ["https://www.googleapis.com/auth/calendar.readonly"];

/**
 * Check if current user has authorized this application.
 */
function checkAuth() {
  gapi.auth.authorize(
    {
      'client_id': CLIENT_ID,
      'scope': SCOPES.join(' '),
      'immediate': true
    }, handleAuthResult);
}

/**
 * Handle response from authorization server.
 *
 * @param {Object} authResult Authorization result.
 */
function handleAuthResult(authResult) {
  var authorizeDiv = document.getElementById('calendar');
  if (authResult && !authResult.error) {
    // Hide auth UI, then load client library.
    loadCalendarApi();
  } else {
    // Just in case of error.
    authorizeDiv.html = "Woops. Looks like there was an issue in authorizing make sure you're logged into your google account."
  }
}

/**
 * Initiate auth flow in response to user clicking authorize button.
 *
 * @param {Event} event Button click event.
 */
function handleAuthClick(event) {
  gapi.auth.authorize(
    {client_id: CLIENT_ID, scope: SCOPES, immediate: false},
    handleAuthResult);
  return false;
}

/**
 * Load Google Calendar client library. List upcoming events
 * once client library is loaded.
 */
function loadCalendarApi() {
  gapi.client.load('calendar', 'v3', listUpcomingEvents);
}

/**
 * Print the summary and start datetime/date of the next ten events in
 * the authorized user's calendar. If no events are found an
 * appropriate message is printed.
 */
function listUpcomingEvents() {

  var request = gapi.client.calendar.events.list({
    'calendarId': 'primary',
    'timeMin': new Date().toISOString(),
    'showDeleted': false,
    'singleEvents': true,
    'orderBy': 'startTime',
  });
  var testArr = [];

  request.execute(function(resp) {
    var events = resp.items;

    if (events.length > 0) {
      for (i = 0; i < events.length; i++) {
        var event = events[i];
        var start = event.start.dateTime;
        var end = event.end.dateTime;
        if (!start) {
          start = event.start.date;
        }

        testArr.push({title: event.summary, allDay: false, start: start, end: end});
      }
    }

    appendPre(testArr);
  });
}

/**
 * Append a pre element to the body containing the given message
 * as its text node.
 *
 * @param {string} message Text to be placed in pre element.
 */


function appendPre(testArr) {
  var myCalendar = $("#calendar").fullCalendar({
      displayEventEnd: {
              month: true
      },
      events: testArr
  });
  // myCalendar.fullCalendar( 'addEventSource', testArr);

  // console.log(testArr);
}
