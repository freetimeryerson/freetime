$('#my-meetings-tab').click(function(){
    $('.meetings').addClass('show');
    $('.profileCal').removeClass('profileCalShow');
});

$('#my-calendar-tab').click(function(){
  $('.meetings').removeClass('show');
  $('.profileCal').addClass('profileCalShow');
})

$('#group-meetings-tab').click(function(){
    $('.meetings').addClass('show');
    $('.groupCal').removeClass('groupCalShow');
    $('.members').removeClass('show');
});

$('#group-calendar-tab').click(function(){
    $('.groupCal').show();
    $('.groupCal').addClass('groupCalShow');
    $('.meetings').removeClass('show');
    $('.members').removeClass('show');
});

$('#group-members-tab').click(function(){
    $('.members').addClass('show');
    $('.groupCal').hide();
    $('.meetings').removeClass('show');
});

// Change Active Background on Tabs
$('.profileNavGroup').on('click', '> li > a', function(a) {
    var $this = $(this);

    $('.profileNavGroup').find('.active').removeClass('active');
    $this.addClass('active');
});

$(function () {
  $('[data-toggle="popover"]').popover()
})
