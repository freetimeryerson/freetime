var express = require('express');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');
var mongodb = require('mongodb');
var Pusher = require('pusher');
var app = express();
var server = require('http').createServer(app);
var configDB = require('./config/database.js');
MongoClient = mongodb.MongoClient;
var db;

var io = require('socket.io')(server);

require('./config/passport.js')(passport);
app.use("/public",express.static(__dirname + "/public"));
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({secret: 'freetime', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.set('view engine','ejs');

var pusher = new Pusher({
  appId: '315935',
  key: '4bf7606d6b203524d576',
  secret: '6da60e78b1a3c1011c2e',
  encrypted: true
});

require('./app/routes/routes.js')(app,passport,pusher);

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';


mongoose.connect(configDB.url, function(err){
  if(err)
    console.log(err);
  else {
    console.log("Successfully connected to database...");
    server.listen(port, ip, function(err){
      if(err)
        console.log(err);
      else {
        console.log('Server running on http://%s:%s', ip, port);
      }
    });
  }
});
